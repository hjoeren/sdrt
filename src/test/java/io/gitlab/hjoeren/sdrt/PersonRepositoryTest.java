package io.gitlab.hjoeren.sdrt;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonRepositoryTest {

  @Autowired
  MockMvc mockMvc;

  @Test
  public void testFindByName() throws Exception {
    mockMvc.perform(get("/viaRepo")
        .param("name", "John"))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void testFindByDateOfBirth() throws Exception {
    mockMvc.perform(get("/viaRepo/search/findByDateOfBirth")
        .param("dateOfBirth", LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void testFindByBirthDate() throws Exception {
    mockMvc.perform(get("/viaRepo/search/findByBirthDate")
        .param("birthDate", LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void testFindByBirthDateWithIsoLocalDateFormat() throws Exception {
    mockMvc.perform(get("/viaRepo")
        .param("birthDate", LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void testFindByBirthDateWithUsFormat() throws Exception {
    mockMvc.perform(get("/viaRepo")
        .param("birthDate", LocalDate.now().format(DateTimeFormatter.ofPattern("MM/dd/yy"))))
        .andDo(print())
        .andExpect(status().isOk());
  }

}
