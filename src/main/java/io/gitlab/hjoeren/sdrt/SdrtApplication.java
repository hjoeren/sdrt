package io.gitlab.hjoeren.sdrt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdrtApplication {

  public static void main(String[] args) {
    SpringApplication.run(SdrtApplication.class, args);
  }

}
