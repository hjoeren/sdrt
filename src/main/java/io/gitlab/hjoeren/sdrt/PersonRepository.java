package io.gitlab.hjoeren.sdrt;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.StringPath;

@RepositoryRestResource(path = "viaRepo")
public interface PersonRepository
    extends JpaRepository<Person, Long>, QuerydslPredicateExecutor<Person>,
    QuerydslBinderCustomizer<QPerson> {

  // Works
  List<Person> findByDateOfBirth(
      @Param("dateOfBirth") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateOfBirth);

  // Don't works
  List<Person> findByBirthDate(@Param("birthDate") LocalDate birthDate);

  @Override
  default void customize(QuerydslBindings bindings, QPerson root) {
    bindings.bind(String.class)
        .first((StringPath path, String value) -> (path.eq(value)));
    bindings.bind(LocalDate.class)
        .first((DatePath<LocalDate> path, LocalDate value) -> (path.eq(value)));
  }

}
