package io.gitlab.hjoeren.sdrt;

import java.time.LocalDate;

import javax.persistence.Entity;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Person extends AbstractPersistable<Long> {

  private String name;

  private LocalDate dateOfBirth;

  private LocalDate birthDate;

}
