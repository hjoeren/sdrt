package io.gitlab.hjoeren.sdrt;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "viaCtrl")
public class PersonController {

  @Autowired
  PersonRepository personRepository;

  @GetMapping(path = "findByDateOfBirth")
  public ResponseEntity<?> findByDateOfBirth(@RequestParam("dateOfBirth") LocalDate dateOfBirth) {
    return ResponseEntity.ok(personRepository.findByDateOfBirth(dateOfBirth));
  }

}
